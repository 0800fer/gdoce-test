# Gdoce Test

Prueba técnica de GDOCE. Se utilizó Python como lenguaje de programación y Django como framework, además de Bootstrap en la plantilla HTML
Aplicación web que dado un número a través de un formulario, realiza una "búsqueda binaria" para encontrar el número ya que se trata de una lista ordenada. Arroja como resultado el número de operaciones realizadas hasta encontrar el número indicado.
El algoritmo en cuestión se encuentra en la vista: gdoce/view.py y se puede ejecutar en la url: localhost/number


