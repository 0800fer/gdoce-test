from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def number(request):
    limite_menor = 1
    limite_mayor = 10000000000
    numero = int(request.GET.get('numero', '0'))
    if not numero:
        return render(request, 'home.html')
    intentos = 0

    while limite_menor <= limite_mayor:
        intentos += 1
        acertar = (limite_mayor + limite_menor) // 2
        if acertar < numero:
            limite_menor = acertar + 1
        elif acertar > numero:
            limite_mayor = acertar - 1
        else:
            return render(request, 'home.html', {'intentos': intentos, 'numero': numero})
    return
