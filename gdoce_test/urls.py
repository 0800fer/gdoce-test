from django.contrib import admin
from django.urls import path

from gdoce import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('number/', views.number, name='number'),
]
